import EventEmitter from "eventemitter3";
import Beat from "./Beat";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();

    this._beat = new Beat();
    this._beat.emit("bit");

    const lyrics = ["Ah", "ha", "ha", "ha", "stayin' alive", "stayin' alive"];
    let count = 0;

    this._beat.on(Beat.events.BIT, () => {
      this._create(lyrics[count]);
      if (count === lyrics.length - 1) {
        count = 0;
      }
      else{
        count += 1;
      }
    });

    this.emit(Application.events.READY);
  } 
  
  _create(lyric) {
    let mainDiv = document.querySelector(".main");    
    const message = document.createElement("div");
    message.classList.add("message");
    message.innerText = lyric;
    mainDiv.appendChild(message);
  }
}
